# simple-messaging

simple-messaging is a gradle project meant to illustrate basic usage of a RabbitMQ message broker.  Some of what's illustrated:

* RabbitMQ between manager and multiple workers
    * Fanout exchange (manager sends message to all workers)
    * Work queue (manager submits jobs - workers accept one job at a time)
* Gradle multi-project structure

## Usage

Clone the repository and execute the following.  This will start a manager app (to submit jobs to), 5 worker apps (to consume the jobs), and a rabbitmq instance.  You can access the RabbitMQ management console at http://localhost:32612.

```
./gradlew install
```

Hit some of the application endpoints by importing the Postman collection in the `postman-collection.json` file.

## License
[MIT](https://choosealicense.com/licenses/mit/)